fun main () {
    var relojBasico = RelojBasico()
    relojBasico.crearFuncionalidad()
    println("\n----------------")

    var relojDeportivo = DecoradorRelojDeportivo(relojBasico)
    relojDeportivo.crearFuncionalidad()
    println("\n----------------")

    var relojLujo = DecoradorRelojLujo(relojBasico)
    relojLujo.crearFuncionalidad()
}

interface Reloj {
    fun crearFuncionalidad()
}

class RelojBasico : Reloj {

    override fun crearFuncionalidad() {
        println("Reloj Basico con: ")
        this.addTimer()
    }

    fun addTimer() {
        print("Temporizador")
    }
}

abstract class DecoradorReloj(private val reloj: Reloj) : Reloj {

    override fun crearFuncionalidad() {
        this.reloj.crearFuncionalidad()
    }
}

class DecoradorRelojDeportivo(reloj: Reloj) : DecoradorReloj(reloj) {

    override fun crearFuncionalidad() {
        super.crearFuncionalidad()
        print(" y mas caracterisctias (Reloj Deportivo): ")
        this.addPedometer()
        this.addSleepMode()
    }

    fun addPedometer() {
        print(" Podometro ")
    }

    fun addSleepMode() {
        print(" SleepMode ")
    }
}

class DecoradorRelojLujo(reloj: Reloj) : DecoradorReloj(reloj) {

    override fun crearFuncionalidad() {
        super.crearFuncionalidad()
        print(" y mas caracteristicas (Reloj Lujo): ")
        this.addFastCharge()
        this.addImpermeability()
        this.addNFC()
    }

    fun addFastCharge() {
        print(" Carga Rapida ")
    }

    fun addImpermeability() {
        print(" Impermeabilidad ")
    }

    fun addNFC() {
        print(" NFC ")
    }
}